/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.protocol.http.control;

import org.apache.jmeter.gui.Stoppable;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

import com.sun.net.httpserver.HttpServer;
import java.net.InetSocketAddress;

/**
 *
 * @author chreid
 */
public class HttpEventServer extends Thread implements Stoppable {
    private static final Logger log = LoggingManager.getLoggerForClass();

    /** The port to listen on. */
    private final int daemonPort;

    /** True if the server is currently running. */
    private volatile boolean running;

    /** Saves the error if one occurs */
    private volatile Exception except;
    private final HttpEventHandler handler;

    public HttpEventServer(HttpEventHandler handler, int port) {
        super("HttpEventServer");
        this.daemonPort = port;
        this.handler = handler;
    }

    @Override
    public void run() {
        except = null;
        running = true;

        try {
            log.info("Creating HttpEvent (monitor) ... on port " + daemonPort);
            // start the server
            HttpServer server = HttpServer.create(new InetSocketAddress(daemonPort), 0);
            server.createContext("/handleEvent", handler);
            server.setExecutor(null);
            server.start();

            handler.serverStarted();

            log.info("HttpEvent up and running!");
            while (running) {
                try {
                    Thread.sleep(1000); // 1 second
                } catch (InterruptedException e) {
                    // wake up!
                }
            }
            server.stop(2); // 2 seconds
            handler.serverStopped();
            log.info("HttpEvent Server stopped");
        } catch (Exception e) {
            except = e;
            log.warn("HttpEvent Server stopped", e);
        }
    }

    @Override
    public void stopServer() {
        running = false;
        this.interrupt(); // stop sleeping
    }

    public Exception getException(){
        return except;
    }
}
