
package com.wmode.jmeter.protocol.http.control.gui;

import com.wmode.jmeter.protocol.http.control.HttpEventCache;
import com.wmode.jmeter.protocol.http.control.HttpEventCacheType;
import com.wmode.jmeter.protocol.http.control.HttpEventControl;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.jmeter.control.gui.LogicControllerGui;

import org.apache.jmeter.gui.JMeterGUIComponent;
import org.apache.jmeter.gui.UnsharedComponent;
import org.apache.jmeter.gui.util.HorizontalPanel;
import org.apache.jmeter.gui.util.MenuFactory;
import org.apache.jmeter.gui.util.VerticalPanel;
import org.apache.jmeter.samplers.Clearable;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jorphan.gui.JLabeledChoice;
import org.apache.jorphan.gui.JLabeledTextField;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

public class HttpEventControlGui extends LogicControllerGui
    implements JMeterGUIComponent, UnsharedComponent, HttpEventCache.HttpEventCacheUpdateListener, Clearable {

    private static final Logger log = LoggingManager.getLoggerForClass();

    private JLabeledTextField portField;
    private JLabeledChoice dbType;
    private JLabeledTextField localPath;
    private JTable resultsList;
    private JScrollPane resultsScrollable;
    private JLabel description;

    private HttpEventControl eventController;


    public HttpEventControlGui() {
        super();
        log.debug("Creating HttpMirrorControlGui");
        init();
    }

    @Override
    public JPopupMenu createPopupMenu() {
        return MenuFactory.getDefaultMenu();
    }

    @Override
    public Collection<String> getMenuCategories() {
        return Arrays.asList(MenuFactory.NON_TEST_ELEMENTS);
    }

    @Override
    public TestElement createTestElement() {
        eventController = new HttpEventControl();
        modifyTestElement(eventController);
        return eventController;
    }

    /**
     * Modifies a given TestElement to mirror the data in the gui components.
     *
     * @see org.apache.jmeter.gui.JMeterGUIComponent#modifyTestElement(TestElement)
     */
    @Override
    public void modifyTestElement(TestElement el) {
        configureTestElement(el);
        if (el instanceof HttpEventControl) {
            eventController = (HttpEventControl) el;
            eventController.setPort(Integer.valueOf(portField.getText()));
            eventController.setDBType(dbType.getText());
            eventController.setDBLocation(localPath.getText());
        }
    }

    @Override
    public String getLabelResource()
    {
        return "httpeventcontrol_title";
    }

    @Override
    public String getStaticLabel(){
        return "HTTP Event Receive Server";
    }

    @Override
    public void configure(TestElement element) {
        log.debug("Configuring gui with " + element);
        super.configure(element);
        eventController = (HttpEventControl) element;
        portField.setText(eventController.getPortString());
        dbType.setText(eventController.getDBType());
        localPath.setText(eventController.getDBLocation());
        repaint();
    }

    private void init() { // WARNING: called from ctor so must not be overridden (i.e. must be private or final)
        setLayout(new BorderLayout(0, 5));
        setBorder(makeBorder());

        add(makeTitlePanel(), BorderLayout.NORTH);

        JPanel mainPanel = new JPanel(new BorderLayout());

        mainPanel.add(createPortPanel(), BorderLayout.NORTH);
        mainPanel.add(createResultsPanel(), BorderLayout.SOUTH);

        add(mainPanel, BorderLayout.CENTER);
    }

    protected String getDescriptionText(){
        // TODO: resource bundle?
        StringBuilder description = new StringBuilder();
        description.append("<html>");
        // TODO: this throws exceptions, we just want the link...
        //URL url = new URL("http", "localhost", Integer.valueOf(portField.getText()), "");
        String url = "http://localhost:"+portField.getText()+"/handleEvent";
        description
            .append("<p>")
            .append("Once tests start server will be available here: ")
            .append("<a href=\"").append(url).append("\">").append(url).append("</a>")
            .append("</p>");
        description
            .append("<p>")
            .append("The main intent of this component is to create a server that will listen during tests<br>")
            .append("for simple HTTP connections.  It is intended to be paired directly with at<br>")
            .append("least one HTTP Event Sampler.")
            .append("</p>");
        description.append("</html>");

        return description.toString();
    }

    protected void refreshDescription(){
        description.setText(getDescriptionText());
        description.invalidate();
    }

    private JPanel createPortPanel() {
        portField = new JLabeledTextField("Port:", 8);
        portField.setName(HttpEventControl.PORT);

        dbType = new JLabeledChoice("Storage Type:", new String[]{
            HttpEventCacheType.TYPE_MEMORY,
            HttpEventCacheType.TYPE_FILE
        });
        localPath = new JLabeledTextField("File Path:");
        localPath.setVisible(false);
        dbType.addChangeListener(new ChangeListener(){
            @Override public void stateChanged(ChangeEvent e){
                boolean visible = dbType.getSelectedIndex() == 1;
                localPath.setVisible(visible);
            }
        });

        VerticalPanel settingsPanel = new VerticalPanel();
        settingsPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Server Settings"));

        description = new JLabel(getDescriptionText());
        JPanel descriptionPanel = new JPanel();
        BoxLayout box = new BoxLayout(descriptionPanel, BoxLayout.PAGE_AXIS);
        descriptionPanel.setLayout(box);
        descriptionPanel.add(description);

        // listen for updates
        List<JComponent> comps = portField.getComponentList();
        JComponent textField = comps.get(comps.size()-1);
        portField.addChangeListener(new ChangeListener(){
            @Override public void stateChanged(ChangeEvent e){
                refreshDescription();
            }
        });
        textField.addKeyListener(new KeyListener(){
            @Override public void keyTyped(KeyEvent e){}
            @Override public void keyPressed(KeyEvent e){}
            @Override public void keyReleased(KeyEvent e){
                refreshDescription();
            }
        });

        settingsPanel.add(portField);
        /*
        There is an issue with trying to enforce this, and tie this and the sampler
        together.
        */
        dbType.setText(HttpEventCacheType.TYPE_MEMORY);
        //settingsPanel.add(dbType);
        //settingsPanel.add(localPath);
        settingsPanel.add(descriptionPanel);
        settingsPanel.add(Box.createVerticalStrut(5));

        return settingsPanel;
    }
    private JPanel createResultsPanel(){
        
        HorizontalPanel panel = new HorizontalPanel();
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Recent Results"));

        try{
            HttpEventCache cache = HttpEventCache.getInstance(HttpEventCacheType.getMemoryInstance());
            cache.addListener(this);
        }catch(ClassNotFoundException e){
            log.error("error trying to get instance for results panel", e);
        }catch(SQLException e){
            log.error("error trying to get instance for results panel", e);
        }
        resultsList = new JTable(new ResultsTableModel());
        resultsScrollable = new JScrollPane(resultsList);
        resultsList.setFillsViewportHeight(true);
        panel.add(resultsScrollable);

        panel.add(Box.createHorizontalStrut(10));

        return panel;
    }

    @Override
    public void clearGui(){
        super.clearGui();
        portField.setText(HttpEventControl.DEFAULT_PORT_S);
        refreshDescription();
        dbType.setSelectedIndex(0);
        localPath.setText(System.getProperty("user.dir"));
    }

    @Override
    public void handleInsert(HttpEventCache.HttpEventCacheUpdateEvent e){
        try{
            // update the UI
            ArrayList<LinkedHashMap<String, Object>> results = e.cache.getAll();
            resultsList.setModel(new ResultsTableModel(results));
            resultsList.invalidate();
        }
        catch (HttpEventCache.NoResulsExecption ex){
            log.error("No results", ex);
            resultsList.setModel(new ResultsTableModel());
        }
        catch (SQLException ex){
            log.error("Error when trying to getAll from cache", ex);
        }
    }

    @Override
    public void handleGet(HttpEventCache.HttpEventCacheUpdateEvent e, ArrayList<LinkedHashMap<String, Object>> results){
        // do nothing
    }

    @Override
    public void handleInit(HttpEventCache.HttpEventCacheUpdateEvent e){
        // do nothing
    }

    @Override
    public void handleDeinit(HttpEventCache.HttpEventCacheUpdateEvent e){
        // do nothing
    }

    @Override
    public void clearData(){
        resultsList.setModel(new ResultsTableModel());
    }
}
