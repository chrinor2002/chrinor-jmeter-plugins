/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.listeners.gui;

import com.wmode.jmeter.listeners.TestRailsListener;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import org.apache.jmeter.gui.util.VerticalPanel;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.visualizers.gui.AbstractListenerGui;
import org.apache.jorphan.gui.JLabeledChoice;
import org.apache.jorphan.gui.JLabeledPasswordField;
import org.apache.jorphan.gui.JLabeledTextField;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

/**
 *
 * @author chreid
 */
public class TestRailsListenerGui extends AbstractListenerGui
{
    private static final Logger log = LoggingManager.getLoggerForClass();

    protected JLabeledTextField testRunID;
    protected JLabeledTextField testRailsUrl;
    protected JLabeledTextField testRailsUser;
    protected JLabeledPasswordField testRailsPassword;
    protected JLabeledTextField testCateIDVariable;
    protected JComboBox<String> testRailsVerbosity;

    public TestRailsListenerGui()
    {
        super();
        init();
    }

    @Override
    public String getLabelResource()
    {
        return "testrailslistner_title";
    }

    @Override
    public String getStaticLabel(){
        return "TestRails Listener";
    }

    @Override
    public void clearGui() {
        super.clearGui();
        testRunID.setText("");
        testRailsUrl.setText("");
        testRailsUser.setText("");
        testRailsPassword.setText("");
        testCateIDVariable.setText("");
        testRailsVerbosity.setSelectedIndex(0);
    }

    @Override
    public void configure(TestElement element)
    {
        super.configure(element);
        TestRailsListener listener = (TestRailsListener) element;
        testRunID.setText(listener.getTestRunId());
        testRailsUrl.setText(listener.getTestRailsBaseUrl());
        testRailsUser.setText(listener.getTestRailsUser());
        testRailsPassword.setText(listener.getTestRailsPassword());
        testCateIDVariable.setText(listener.getTestCaseIdVariable());
        testRailsVerbosity.setSelectedItem(listener.getTestRailsVerbosity());
    }

    @Override
    public TestElement createTestElement()
    {
        TestRailsListener summariser = new TestRailsListener();
        modifyTestElement(summariser);
        return summariser;
    }

    @Override
    public void modifyTestElement(TestElement element)
    {
        super.configureTestElement(element);
        if (element instanceof TestRailsListener) {
            TestRailsListener listener = (TestRailsListener) element;
            listener.setTestRunId(testRunID.getText());
            listener.setTestRailsBaseUrl(testRailsUrl.getText());
            listener.setTestRailsUser(testRailsUser.getText());
            listener.setTestRailsPassword(testRailsPassword.getText());
            listener.setTestCaseIdVariable(testCateIDVariable.getText());
            listener.setTestRailsVerbosity((String)testRailsVerbosity.getSelectedItem());
        }
    }

    protected void init()
    {
        setLayout(new BorderLayout());
        setBorder(makeBorder());

        add(makeTitlePanel(), BorderLayout.NORTH);

        VerticalPanel panel = new VerticalPanel();
        panel.setBorder(BorderFactory.createEmptyBorder());

        testRunID = new JLabeledTextField("Test Run ID: ");
        testRailsUrl = new JLabeledTextField("Test Rails URL: ");
        testRailsUser = new JLabeledTextField("Test Rails User: ");
        testRailsPassword = new JLabeledPasswordField("Test Rails Password: ");
        testCateIDVariable = new JLabeledTextField("Test Case ID Variable: ");
        testRailsVerbosity = new JComboBox<String>(new String[]{
            TestRailsListener.TR_VERBOSITY_FAILURES_AND_PASSES,
            TestRailsListener.TR_VERBOSITY_FAILURES,
            TestRailsListener.TR_VERBOSITY_PASSES,
            TestRailsListener.TR_VERBOSITY_NONE,
        });
        testRailsVerbosity.setEditable(true);
        panel.add(testRunID);
        panel.add(testRailsUrl);
        panel.add(testRailsUser);
        panel.add(testRailsPassword);
        panel.add(testCateIDVariable);
        panel.add(testRailsVerbosity);

        add(panel);
    }
}
