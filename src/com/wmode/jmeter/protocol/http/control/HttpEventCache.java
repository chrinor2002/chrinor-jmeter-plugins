/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.protocol.http.control;

import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.function.Consumer;
import java.util.logging.Level;
import org.apache.commons.io.IOUtils;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author chreid
 */
public class HttpEventCache {
    private static final Logger log = LoggingManager.getLoggerForClass();

    protected ArrayList<HttpEventCacheUpdateListener> listeners = new ArrayList<>();

    protected static HashMap<HttpEventCacheType, HttpEventCache> instances = new HashMap<>();

    protected Connection c;

    protected HttpEventCacheType type;

    private HttpEventCache() throws ClassNotFoundException, SQLException{
        this(HttpEventCacheType.getMemoryInstance());
    }
    private HttpEventCache(HttpEventCacheType type) throws ClassNotFoundException, SQLException{
        this.type = type;
        init();
    }

    public static HttpEventCache getInstance(HttpEventCacheType type) throws ClassNotFoundException, SQLException{
        HttpEventCache instance = instances.get(type);
        if(instance == null){
            instance = new HttpEventCache(type);
            instances.put(type, instance);
        }
        return instance;
    }

    public void addListener(HttpEventCacheUpdateListener listener){
        listeners.add(listener);
    }
    public void removeListener(HttpEventCacheUpdateListener listener){
        listeners.remove(listener);
    }

    private Connection getConnection() throws SQLException{
        String connection = "jdbc:sqlite:" + this.type.location;
        return DriverManager.getConnection(connection);
    }

    private void init() throws ClassNotFoundException, SQLException {
        // get a connection to memory sqlite
        Class.forName("org.sqlite.JDBC");
        c = getConnection();
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable(){
            @Override public void run(){
                try{
                    deinit();
                }
                catch (SQLException ex){
                    log.error("error on shutdown of HttpEventCache", ex);
                }
            }
        }));

        try{
            Statement stmt = stmt = c.createStatement();
            String sql = "DROP TABLE requests;";
            stmt.executeUpdate(sql);
            stmt.close();
        }catch(SQLException ex){
            // assume this is "table does not exist, and ignore it.
            log.debug("Error while deleting requests table.", ex);
        }

        Statement stmt = stmt = c.createStatement();
        String sql = "CREATE TABLE requests " +
                     "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                     "mtime INTEGER, " + 
                     "protocol TEXT, " + 
                     "method TEXT, " + 
                     "requestedHost TEXT, " + 
                     "requestedPort INTEGER, " + 
                     "remoteHostName TEXT, " + 
                     "requestHeaders TEXT, " + 
                     "requestBody BLOB)";
        stmt.executeUpdate(sql);
        stmt.close();

        HttpEventCache self = this;
        listeners.forEach(new Consumer<HttpEventCacheUpdateListener>() {
            @Override
            public void accept(HttpEventCacheUpdateListener listener){
                HttpEventCacheUpdateEvent e = new HttpEventCacheUpdateEvent();
                e.cache = self;
                listener.handleInit(e);
            }
        });
    }
    
    private void deinit() throws SQLException {
        c.close();

        HttpEventCache self = this;
        listeners.forEach(new Consumer<HttpEventCacheUpdateListener>() {
            @Override
            public void accept(HttpEventCacheUpdateListener listener){
                HttpEventCacheUpdateEvent e = new HttpEventCacheUpdateEvent();
                e.cache = self;
                listener.handleDeinit(e);
            }
        });
    }

    public static String getHttpExchangeBodyString(HttpExchange t) throws IOException {
        InputStream in = t.getRequestBody();
        StringWriter writer = new StringWriter();
        InputStreamReader reader = new InputStreamReader(in);
        IOUtils.copy(reader, writer);
        String body = writer.toString();
        return body;
    }

    public boolean put(String protocol, String method, String remoteHost, HttpExchange t){
        boolean status = true;
        String headers = JSONObject.toJSONString(t.getRequestHeaders());
        try{
            String[] fields = new String[]{
                "mtime",
                "protocol",
                "method",
                "requestedHost",
                "requestedPort",
                "remoteHostName",
                "requestHeaders",
                "requestBody"
            };
            StringBuilder sql = new StringBuilder("INSERT INTO requests(");
            sql.append(String.join(", ", fields));
            sql.append(") VALUES( ");
            Arrays.fill(fields, "?");
            sql.append(String.join(", ", fields));
            sql.append(" );");

            log.debug("attempting to insert using: " + sql.toString());
            PreparedStatement stmt = c.prepareStatement(sql.toString());
            Date now = new Date();
            stmt.setLong(1, now.getTime());
            stmt.setString(2, protocol);
            stmt.setString(3, method);
            stmt.setString(4, t.getLocalAddress().getHostName());
            stmt.setInt(5, t.getLocalAddress().getPort());
            stmt.setString(6, remoteHost);
            stmt.setString(7, headers);
            byte[] bytes = IOUtils.toByteArray(t.getRequestBody());
            stmt.setBytes(8, bytes);
            int count = stmt.executeUpdate();
            log.debug("updated/inserted: " + Integer.toString(count));
            
            HttpEventCache self = this;
            listeners.forEach(new Consumer<HttpEventCacheUpdateListener>() {
                @Override
                public void accept(HttpEventCacheUpdateListener listener){
                    HttpEventCacheUpdateEvent e = new HttpEventCacheUpdateEvent();
                    e.cache = self;
                    listener.handleInsert(e);
                }
            });

        }catch(SQLException e){
            log.error("unable to insert row", e);
            status = false;
        }finally{
            return status;
        }
    }

    public int clearAll() throws SQLException{
        PreparedStatement stmt = c.prepareStatement("DELETE FROM requests;");
        int rows = stmt.executeUpdate();
        return rows;
    }

    public ArrayList<LinkedHashMap<String, Object>> getAll() throws SQLException, NoResulsExecption {
        String sql = "SELECT * FROM requests;";
        return get(sql, new ArrayList<>());
    }
    public LinkedHashMap<String, Object> get() throws SQLException, NoResulsExecption {
        return getOne(null, null, null, null);
    }
    public LinkedHashMap<String, Object> getOne(String protocol, String method, String remoteHostName, String bodyContains) throws SQLException, NoResulsExecption {
        StringBuilder sql = new StringBuilder("SELECT * FROM requests");
        ArrayList<String> conditions = new ArrayList<>();
        ArrayList<String> args = new ArrayList<>();
        if (protocol != null){
            conditions.add("protocol = ?");
            args.add(protocol);
        }
        if (method != null){
            conditions.add("method = ?");
            args.add(method);
        }
        if (remoteHostName != null){
            conditions.add("remoteHostName = ?");
            args.add(remoteHostName);
        }
        if (bodyContains != null){
            conditions.add("hex(requestBody) LIKE '%' || hex(?) || '%'");
            args.add(bodyContains);
        }

        // conditions
        if (!conditions.isEmpty()){
            sql.append(" WHERE ");
            sql.append(String.join(" AND ", conditions));
        }
        // limit
        sql.append(" LIMIT 1");
        // end
        sql.append(";");

        return get(sql.toString(), args).get(0);
    }
    protected ArrayList<LinkedHashMap<String, Object>> get(String sql, ArrayList<String> args) throws SQLException, NoResulsExecption {
        log.debug("attempting to select using:" + sql);
        log.debug("sql arguments:" + args.toString());
        PreparedStatement stmt = c.prepareStatement(sql);
        int index = 1;
        for(Iterator<String> i = args.iterator(); i.hasNext();){
            stmt.setString(index++, i.next());
        }
        ResultSet results = stmt.executeQuery();
        ResultSetMetaData metadata = results.getMetaData();
        int numberOfColumns = metadata.getColumnCount();
        ArrayList<LinkedHashMap<String, Object>> resultz = new ArrayList<LinkedHashMap<String, Object>>();
        while(results.next()){
            LinkedHashMap<String, Object> row = new LinkedHashMap<String, Object>();
            for(int i = 1; i <= numberOfColumns; i++){
                String key = metadata.getColumnName(i);
                Object value = results.getObject(i);
                row.put(key, value);
            }
            resultz.add(row);
        }
        HttpEventCache self = this;
        listeners.forEach(new Consumer<HttpEventCacheUpdateListener>() {
            @Override
            public void accept(HttpEventCacheUpdateListener listener){
                HttpEventCacheUpdateEvent e = new HttpEventCacheUpdateEvent();
                e.cache = self;
                listener.handleGet(e, resultz);
            }
        });
        if (resultz.size() > 0){
            return resultz;
        }else{
            throw new NoResulsExecption("No results from cache.");
        }
    }
    public static class NoResulsExecption extends Exception {
        public NoResulsExecption(String message){
            super(message);
        }
    }

    public static interface HttpEventCacheUpdateListener{
        public void handleInsert(HttpEventCacheUpdateEvent e);
        public void handleGet(HttpEventCacheUpdateEvent e, ArrayList<LinkedHashMap<String, Object>> results);
        public void handleInit(HttpEventCacheUpdateEvent e);
        public void handleDeinit(HttpEventCacheUpdateEvent e);
    }
    public static class HttpEventCacheUpdateEvent{
        public HttpEventCache cache;
    }
}
