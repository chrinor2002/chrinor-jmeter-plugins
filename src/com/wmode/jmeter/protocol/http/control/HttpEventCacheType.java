package com.wmode.jmeter.protocol.http.control;

/**
 *
 * @author chreid
 */
public class HttpEventCacheType
{
    public static final String TYPE_MEMORY = ":memory:";
    public static final String TYPE_FILE = ":file:";

    protected String location;

    protected HttpEventCacheType(String location){
        this.location = location;
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof HttpEventCacheType){
            HttpEventCacheType two = (HttpEventCacheType)obj;
            return two.location.equals(location);
        }
        else{
            return false;
        }
    }

    @Override
    public int hashCode(){
        return location.hashCode();
    }

    
    
    public static HttpEventCacheType getMemoryInstance(){
        return new HttpEventCacheType(":memory:");
    }
    public static HttpEventCacheType getFileInstance(String location){
        return new HttpEventCacheType(location);
    }
}
