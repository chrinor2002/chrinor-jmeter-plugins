/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.protocol.http.control;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.apache.jmeter.control.ModuleController;
import org.apache.jmeter.gui.GuiPackage;
import org.apache.jmeter.samplers.Clearable;
import org.apache.jmeter.testelement.AbstractTestElement;
import org.apache.jmeter.testelement.TestStateListener;
import org.apache.jmeter.testelement.property.IntegerProperty;
import org.apache.jmeter.testelement.property.StringProperty;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

/**
 *
 * @author chreid
 */
public class HttpEventControl extends AbstractTestElement
        implements TestStateListener, Clearable {
    protected static final Logger log = LoggingManager.getLoggerForClass();

    private transient HttpEventServer server;
    private transient HttpEventHandler handler;

    public static final int DEFAULT_PORT = 8081;

    // and as a string
    public static final String DEFAULT_PORT_S = Integer.toString(DEFAULT_PORT);

    public static final String PORT = "HttpEventControlGui.port";
    public static final String DB_TYPE = "HttpEventControlGui.db_type";
    public static final String DB_LOCATION = "HttpEventControlGui.db_location";

    public HttpEventControl() {
        initPort(DEFAULT_PORT);
    }

    protected void initPort(int port){
        setProperty(new IntegerProperty(PORT, port));
    }
    public void setPort(int port) {
        initPort(port);
    }
    public int getPort() {
        return getPropertyAsInt(PORT);
    }
    public String getPortString() {
        return getPropertyAsString(PORT);
    }

    protected void initDBType(String type){
        setProperty(new StringProperty(DB_TYPE, type));
    }
    public void setDBType(String type) {
        initDBType(type);
    }
    public String getDBType() {
        return getPropertyAsString(DB_TYPE);
    }

    protected void initDBLocation(String location){
        setProperty(new StringProperty(DB_LOCATION, location));
    }
    public void setDBLocation(String location) {
        initDBLocation(location);
    }
    public String getDBLocation() {
        return getPropertyAsString(DB_LOCATION);
    }
    
    public void startHttpEventMonitor() {
        HttpEventCache cache;
        try{
            if(getDBType().equals(HttpEventCacheType.TYPE_FILE)){
                cache = HttpEventCache.getInstance(HttpEventCacheType.getFileInstance(getDBLocation()));
            }else{
                cache = HttpEventCache.getInstance(HttpEventCacheType.getMemoryInstance());
            }
            handler = new HttpEventHandler(cache);
            server = new HttpEventServer(handler, getPort());
            log.info("Starting new HttpEventServer " + this);
            server.start();
            GuiPackage instance = GuiPackage.getInstance();
            if (instance != null) {
                instance.register(server);
            }
        }catch(ClassNotFoundException ex){
            log.error("error trying to start http event server", ex);
        }catch(SQLException ex){
            log.error("error trying to start http event server", ex);
        }
    }

    public void stopHttpEventMonitor() {
        if (server != null) {
            log.info("stopping HttpEventServer " + this);
            server.stopServer();
            GuiPackage instance = GuiPackage.getInstance();
            if (instance != null) {
                instance.unregister(server);
            }
            try {
                server.join(1000); // wait for server to stop
            }
            catch (InterruptedException e) {
            }
            server = null;
        }
    }

    @Override
    public boolean canRemove() {
        return null == server;
    }

    public boolean isServerAlive(){
        return server != null && server.isAlive();
    }

    @Override
    public void testStarted()
    {
        testStarted("local");
    }

    @Override
    public void testStarted(String string)
    {
        // start server
        startHttpEventMonitor();
    }

    @Override
    public void testEnded()
    {
        testEnded("local");
    }

    @Override
    public void testEnded(String string)
    {
        stopHttpEventMonitor();
    }

    /*
    Needed to be found to be clearable.
    */
    @Override
    public void clearData(){
    }
}
