/**
 * TestRail API binding for Java (API v2, available since TestRail 3.0)
 *
 * Learn more:
 *
 * http://docs.gurock.com/testrail-api2/start
 * http://docs.gurock.com/testrail-api2/accessing
 *
 * Copyright Gurock Software GmbH. See license.md for details.
 */

package com.gurock.testrail;
 
import java.net.HttpURLConnection;

public class APIException extends Exception
{
    protected HttpURLConnection conn = null;
    protected int code = -1;

	public APIException(String message)
	{
		super(message);
	}
	public APIException(String message, int code, HttpURLConnection conn)
	{
		super(message);
        this.conn = conn;
	}

    public int getHTTPCode(){
        return this.code;
    }
    public HttpURLConnection getHttpURLConnection(){
        return conn;
    }
}
