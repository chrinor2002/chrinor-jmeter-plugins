# Concept
Every once in a while as problem arrises where the tools already available do not quite meet the needs of your team. This package is intended to serve as a way for me to provide some of those tools to the teams I work with.

# Plugins:
* ChrinorRAWHttpSampler - This class exactly matches http://jmeter-plugins.org/wiki/RawRequest/  With a minor addition to store a file in base64, rather than a path.  This allows a test to be more self contained within a jmx file.