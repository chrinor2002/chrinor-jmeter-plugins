/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.samples;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.text.NumberFormat;
import java.text.ParseException;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.NumberFormatter;
import org.apache.jmeter.gui.util.VerticalPanel;
import org.apache.jmeter.samplers.gui.AbstractSamplerGui;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

/**
 *
 * @author chreid
 */
public class HttpEventSamplerGui extends AbstractSamplerGui {
    protected static final Logger log = LoggingManager.getLoggerForClass();

    protected JSampleOptionField protocol;
    protected JSampleOptionField method;
    protected JSampleOptionField remoteHost;
    protected JSampleOptionField bodyContains;
    protected JSampleOptionField timeout;
    protected JSampleOptionField queryInterval;
    
    /**
     *
     */
    public HttpEventSamplerGui() {
        init();
        initFields();
    }

    @Override
    public String getStaticLabel() {
        return "HTTP Event Sampler";
    }

    @Override
    public void configure(TestElement element) {
        super.configure(element);

        if (element instanceof HttpEventSampler) {
            HttpEventSampler rawSampler = (HttpEventSampler) element;
            this.protocol.setSelected(rawSampler.getUseProtocol());
            this.protocol.setText(rawSampler.getProtocol());
            this.method.setSelected(rawSampler.getUseMethod());
            this.method.setText(rawSampler.getMethod());
            this.remoteHost.setSelected(rawSampler.getUseRemoteHost());
            this.remoteHost.setText(rawSampler.getRemoteHost());
            this.bodyContains.setSelected(rawSampler.getUseBodyContains());
            this.bodyContains.setText(rawSampler.getBodyContains());
            this.timeout.setText(rawSampler.getTimeoutAsString());
            this.queryInterval.setText(rawSampler.getQueryIntervalAsString());
        }
    }

    @Override
    public TestElement createTestElement() {
        HttpEventSampler sampler = new HttpEventSampler();
        modifyTestElement(sampler);
        return sampler;
    }

    /**
     * Modifies a given TestElement to mirror the data in the gui components.
     *
     * @param sampler
     * @see org.apache.jmeter.gui.JMeterGUIComponent#modifyTestElement(TestElement)
     */
    @Override
    public void modifyTestElement(TestElement sampler) {
        super.configureTestElement(sampler);

        if (sampler instanceof HttpEventSampler) {
            HttpEventSampler rawSampler = (HttpEventSampler) sampler;
            rawSampler.setUseProtocol(protocol.isSelected());
            rawSampler.setProtocol(protocol.getText());
            rawSampler.setUseMethod(method.isSelected());
            rawSampler.setMethod(method.getText());
            rawSampler.setUseRemoteHost(remoteHost.isSelected());
            rawSampler.setRemoteHost(remoteHost.getText());
            rawSampler.setUseBodyContains(bodyContains.isSelected());
            rawSampler.setBodyContains(bodyContains.getText());
            try{
                int timeoutValue = IntegerOnlyFormatter.valueFromString(timeout.textfield, timeout.getText());
                rawSampler.setTimeout(timeoutValue);
            }
            catch (ParseException ex){
                log.error("Error setting value for timeout.", ex);
            }
            try{
                int queryIntervalValue = IntegerOnlyFormatter.valueFromString(queryInterval.textfield, queryInterval.getText());
                rawSampler.setQueryInterval(queryIntervalValue);
            }
            catch (ParseException ex){
                log.error("Error setting value for queryInterval.", ex);
            }
            
        }
    }

    @Override
    public void clearGui() {
        super.clearGui();
        initFields();
    }

    @Override
    public String getLabelResource() {
        return this.getClass().getSimpleName();
    }

    protected String getDescriptionText(){
        StringBuilder description = new StringBuilder();
        description.append("<html>");
        description
            .append("<p>")
            .append("This sampler works by querying a running 'HTTP Event Receive Server'<br>")
            .append("The server stores requests as needed, and this sampler will fetch<br>")
            .append("those requests based on query settings.  If no incoming request is found<br>")
            .append("a timeout occurs.  This is equivalent to a server timeout when<br>")
            .append("making a normal HTTP request.")
            .append("</p>");
        description.append("<hr>");
        description
            .append("<p>")
            .append("If at least one http event is found, it is returned to the sampler.<br>")
            .append("The body, and header are presented as though the request <br>")
            .append("was actually a response from an HTTP query.  As a result, this sampler<br>")
            .append("should be treated as though it was a response from a server,<br>")
            .append("rather than a incoming request.  Assertions should be applied in<br>")
            .append("the same way as other samplers, keeping in mind<br>")
            .append("that the body of this sample, was the incoming request body, not an<br>")
            .append("actual server response.")
            .append("</p>");
        description.append("</html>");

        return description.toString();
    }
    
    protected void init() {
        setLayout(new BorderLayout(0, 5));
        setBorder(makeBorder());

        add(makeTitlePanel(), BorderLayout.NORTH);

        VerticalPanel mainPanel = new VerticalPanel();
        mainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Query Settings"));

        mainPanel.setToolTipText("Use these settings to control what sort of request you are looking for.");

        // options
        mainPanel.add(protocol = new JSampleOptionField("Protocol:"));
        protocol.setToolTipText("Wait for a connection with this protocol.");
        mainPanel.add(method = new JSampleOptionField("Method:"));
        method.setToolTipText("Wait for a connection with this method.");
        mainPanel.add(remoteHost = new JSampleOptionField("Remote Host:"));
        method.setToolTipText("Wait for a connection from this host.");
        mainPanel.add(bodyContains = new JSampleOptionField("Body Contains:"));
        bodyContains.setToolTipText("Filter requests by this string being in the body.");
        mainPanel.add(timeout = new JSampleOptionField("Timeout(ms):", false));
        timeout.setToolTipText("Wait for this many miliseconds.");
        timeout.textfield.setFormatterFactory(new IntegerOnlyFormatter());
        mainPanel.add(queryInterval = new JSampleOptionField("Query Interval(ms):", false));
        queryInterval.setToolTipText("Wait this long between query attempts.");
        queryInterval.textfield.setFormatterFactory(new IntegerOnlyFormatter());

        // description
        mainPanel.add(new JLabel(getDescriptionText()));
        mainPanel.add(Box.createVerticalStrut(5));

        JPanel container = new JPanel(new BorderLayout());
        container.add(mainPanel, BorderLayout.NORTH);
        add(container, BorderLayout.CENTER);
    }

    protected void initFields() {
        protocol.setSelected(true);
        protocol.setText("http");
        method.setSelected(true);
        method.setText("POST");
        remoteHost.setSelected(true);
        remoteHost.setText("localhost");
        bodyContains.setSelected(false);
        bodyContains.setText("");
        timeout.setText("30001");
        queryInterval.setText("250");
    }

    protected static class IntegerOnlyFormatter extends JFormattedTextField.AbstractFormatterFactory {
        @Override public JFormattedTextField.AbstractFormatter getFormatter(JFormattedTextField tf){
            NumberFormat format = NumberFormat.getInstance();
            NumberFormatter formatter = new NumberFormatter(format);
            formatter.setAllowsInvalid(false);
            formatter.setValueClass(Integer.class);
            formatter.setMinimum(0);
            formatter.setMaximum(Integer.MAX_VALUE);
            // If you want the value to be committed on each keystroke instead of focus lost
            formatter.setCommitsOnValidEdit(true);
            return formatter;
        }
        public static int valueFromString(JFormattedTextField tf, String input) throws ParseException{
            // safe to cast
            return (int)tf.getFormatter().stringToValue(input);
        }
    }
    protected static class JSampleOptionField extends JPanel {
        public JCheckBox checkbox = new JCheckBox();
        public JLabel label = new JLabel();
        public JFormattedTextField textfield = new JFormattedTextField();

        public JSampleOptionField(String label){
            this(label, true);
        }
        public JSampleOptionField(String labelText, boolean useCheckbox){
            this.setLayout(new GridBagLayout());
            label.setText(labelText);
            if (useCheckbox){
                checkbox.setToolTipText("Enable this option.");
            }

            label.setLabelFor(textfield);

            GridBagConstraints checkContraints = new GridBagConstraints();

            GridBagConstraints labelContraints = new GridBagConstraints();

            GridBagConstraints fieldContraints = new GridBagConstraints();
            fieldContraints.weightx = 1;
            fieldContraints.fill = GridBagConstraints.HORIZONTAL;
            
            if (useCheckbox){
                this.add(checkbox, checkContraints);
            }
            this.add(label, labelContraints);
            this.add(textfield, fieldContraints);
        }

        public void setLabel(String text){
            label.setText(text);
        }
        public String getLabel(){
            return label.getText();
        }

        public void setText(String text){
            textfield.setText(text);
        }
        public String getText(){
            return textfield.getText();
        }

        public void setSelected(boolean selected){
            checkbox.setSelected(selected);
        }
        public boolean isSelected(){
            return checkbox.isSelected();
        }
    }
}
