/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.listeners;

import java.util.HashMap;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.jmeter.samplers.SampleEvent;
import org.apache.jmeter.samplers.SampleListener;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.testelement.AbstractTestElement;
import org.apache.jmeter.testelement.TestStateListener;
import org.apache.jmeter.threads.JMeterContext;
import org.apache.jmeter.threads.JMeterVariables;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

/**
 * NOTE: the instance used in testStateListeners is not the same instance as the one that listens for samples
 * This is why the sampleGroups is setup as a static.
 * @author chreid
 */
public class TestRailsListener extends AbstractTestElement
    implements SampleListener, TestStateListener
{
    private static final Logger log = LoggingManager.getLoggerForClass();

    protected static String lastTestId = null;
    private static final String PROPERTY_KEY_TR_BASE_URL = "TR_BASE_URL";
    private static final String PROPERTY_KEY_TR_USER = "TR_USER";
    private static final String PROPERTY_KEY_TR_PASSWORD = "TR_PASSWORD";
    private static final String PROPERTY_KEY_TESTRUN_ID = "TESTRUN_ID";
    private static final String PROPERTY_KEY_TESTCASE_ID_VAR = "TESTCASE_ID_VAR";
    private static final String PROPERTY_KEY_TR_VERBOSITY = "TR_VERBOSITY";
    public static final String TR_VERBOSITY_NONE = "NONE";
    public static final String TR_VERBOSITY_FAILURES = "FAILURES";
    public static final String TR_VERBOSITY_PASSES = "PASSES";
    public static final String TR_VERBOSITY_FAILURES_AND_PASSES = "FAILS_AND_PASSES";
    protected static HashMap<String, TestRailsRunShipmentGroup> sampleGroups = new HashMap();

    /*
    TODO: this needs to be thought through a bit more... The object instance that
    listens for samples is NOT the same object at the one that listens for test states
    as a result samples that are queued at the end of the test are not shipped to testrails
    In theory this should solve that problem, but I do not have time to test this and ensure it
    will do exactly as I expect, for now the sampleGroups and lastTestId are going to be static
    
    @Override
    public Object clone(){
        TestRailsListener clone = (TestRailsListener) super.clone();
        clone.lastTestId = lastTestId;
        clone.sampleGroups = this.sampleGroups;
        return clone;
    }
    */

    public void setTestRailsBaseUrl(String base_url)
    {
        setProperty(PROPERTY_KEY_TR_BASE_URL, base_url);
    }

    public String getTestRailsBaseUrl()
    {
        return getPropertyAsString(PROPERTY_KEY_TR_BASE_URL);
    }

    public void setTestRailsUser(String user)
    {
        setProperty(PROPERTY_KEY_TR_USER, user);
    }

    public String getTestRailsUser()
    {
        return getPropertyAsString(PROPERTY_KEY_TR_USER);
    }

    public void setTestRailsPassword(String pass)
    {
        setProperty(PROPERTY_KEY_TR_PASSWORD, pass);
    }

    public String getTestRailsPassword()
    {
        return getPropertyAsString(PROPERTY_KEY_TR_PASSWORD);
    }

    public String getTestRunId()
    {
        return getPropertyAsString(PROPERTY_KEY_TESTRUN_ID);
    }
    public void setTestRunId(String testRunId)
    {
        setProperty(PROPERTY_KEY_TESTRUN_ID, testRunId);
    }

    public String getTestCaseIdVariable()
    {
        return getPropertyAsString(PROPERTY_KEY_TESTCASE_ID_VAR);
    }
    public void setTestCaseIdVariable(String testCaseIdVar)
    {
        setProperty(PROPERTY_KEY_TESTCASE_ID_VAR, testCaseIdVar);
    }

    public String getTestRailsVerbosity()
    {
        return getPropertyAsString(PROPERTY_KEY_TR_VERBOSITY);
    }

    public void setTestRailsVerbosity(String testVerbosity)
    {
        setProperty(PROPERTY_KEY_TR_VERBOSITY, testVerbosity);
    }

    protected boolean sanityCheck(){
        // check that we have a valid test run
        String runID = getTestRunId();
        boolean validRunID = !runID.isEmpty() && NumberUtils.isNumber(runID);
        if (!validRunID){
            log.warn("Test Run ID '" + runID + "' appears to be invalid.  Check confgiguration.");
        }

        // check we have a valid url
        String baseUrl = getTestRailsBaseUrl();
        boolean validBaseUrl = !baseUrl.isEmpty();
        if (!validBaseUrl){
            log.warn("Test Rails URL '" + baseUrl + "' appears to be invalid.  Check confgiguration.");
        }

        // check we have a valid user
        String user = getTestRailsUser();
        boolean validUser = !user.isEmpty();
        if (!validUser){
            log.warn("Test Rails User '" + user + "' appears to be invalid.  Check confgiguration.");
        }

        // check we have a valid password
        String password = getTestRailsPassword();
        boolean validPassword = !password.isEmpty();
        if (!validPassword){
            log.warn("Test Rails Password '" + password + "' appears to be invalid.  Check confgiguration.");
        }
        
        // check that test ID variable is valid and does not reference an empty variable
        String caseIdVariableName = getTestCaseIdVariable();
        boolean validVariable = !caseIdVariableName.isEmpty();
        if (validVariable){
            JMeterContext ctx = getThreadContext();
            JMeterVariables vars = ctx.getVariables();
            String testRailTestCaseID = vars.get(caseIdVariableName);
            validVariable = validVariable && !testRailTestCaseID.isEmpty();
            if (!validVariable){
                log.warn("The variable '" + caseIdVariableName + "' does not appear to reference a valid variable value: '" + testRailTestCaseID + "'.  Check confgiguration.");
            }
        }else{
            log.warn("Test Rails Case ID '" + caseIdVariableName + "' variable appears to be invalid.  Check confgiguration.");
        }

        return validRunID && validBaseUrl && validUser && validPassword && validVariable;
    }

    @Override
    public void sampleOccurred(SampleEvent se)
    {
        // initial sanity check to prevent extra processing
        String runID = getTestRunId();
        if (runID.isEmpty()){
            log.debug("Test Run ID '" + runID + "' appears to be empty. Not attempting to log to Test Rails.");
            return;
        }else if(!NumberUtils.isNumber(runID)){
            log.debug("Test Run ID '" + runID + "' appears to not be a number. Not attempting to log to Test Rails.");
            return;
        }
        
        // second sanity check
        boolean shouldProcess = sanityCheck();
        if (!shouldProcess){
            log.debug("Sanity check failed, not attempting to log to Test Rails.");
            return;
        }

        JMeterContext ctx = getThreadContext();
        JMeterVariables vars = ctx.getVariables();
        SampleResult sampleResult = se.getResult();

        final String testRailTestRunID = getTestRunId();
        final String testRailTestCaseID = vars.get(getTestCaseIdVariable());
        log.debug("Setting up Runnable for TestRail TestRun " + testRailTestRunID + " and TestCase ID " + testRailTestCaseID + " for " + sampleResult.getSampleLabel(true) + ".");

        // check to see if this is a different testID than the last event
        log.debug("testRailTestCaseID:" + testRailTestCaseID);
        log.debug("lastTestId:" + lastTestId);
        log.debug("my isntance:" + this.toString());
        if (lastTestId != null && !testRailTestCaseID.equals(lastTestId))
        {
            processGroupKey(lastTestId);
        }

        // make sure we have a group list to work with
        if (!sampleGroups.containsKey(testRailTestCaseID))
        {
            TestRailsRunShipmentGroup group = new TestRailsRunShipmentGroup(testRailTestRunID, testRailTestCaseID);
            group.setupAPI(getTestRailsBaseUrl(), getTestRailsUser(), getTestRailsPassword());
            int passedVerbosity = TestRailsRunShipmentGroup.TOGGLE_LOGGIN_NONE;
            String verb = getTestRailsVerbosity();
            switch(verb){
                
                case TR_VERBOSITY_PASSES:
                    passedVerbosity = TestRailsRunShipmentGroup.TOGGLE_LOGGING_PASSES_DETAILS;
                    break;

                case TR_VERBOSITY_FAILURES:
                    passedVerbosity = TestRailsRunShipmentGroup.TOGGLE_LOGGING_FAILURE_DETAILS;
                    break;

                case TR_VERBOSITY_FAILURES_AND_PASSES:
                    passedVerbosity = TestRailsRunShipmentGroup.TOGGLE_LOGGING_FAILURE_DETAILS | TestRailsRunShipmentGroup.TOGGLE_LOGGING_PASSES_DETAILS;
                    break;

                case TR_VERBOSITY_NONE:
                default:
                    // do nothing...
            }
            group.setVerbosity(passedVerbosity);
            sampleGroups.put(testRailTestCaseID, group);
        }

        // add the current sample results to the grouping
        TestRailsRunShipment shipment = new TestRailsRunShipment(sampleResult);
        sampleGroups.get(testRailTestCaseID).manifest.add(shipment);

        // set the fact that this current testID was the last one we processed.
        lastTestId = testRailTestCaseID;

        return;
    }

    protected void processGroupKey(String key){
        log.debug("Processing shipment group.  Starting new thread.");
        log.debug("lastTestId:" + lastTestId);
        log.debug("my isntance:" + this.toString());
        // only take action if there is anything in the hashes.
        if (sampleGroups.containsKey(lastTestId)){
            TestRailsRunShipmentGroup group = sampleGroups.get(lastTestId);
            sampleGroups.remove(lastTestId); // clean up references

            // run the guts
            group.run();
        }
    }

    @Override
    public void sampleStarted(SampleEvent se)
    {
        // do nothing, not used
    }

    @Override
    public void sampleStopped(SampleEvent se)
    {
        // do nothing, not used
    }

    @Override
    public void testStarted()
    {
        testStarted("local");
    }

    @Override
    public void testStarted(String string)
    {
        // do nothing, not used
    }

    @Override
    public void testEnded()
    {
        testEnded("local");
    }

    @Override
    public void testEnded(String string)
    {
        // ship over the last stragglers
        processGroupKey(lastTestId);
    }

}
