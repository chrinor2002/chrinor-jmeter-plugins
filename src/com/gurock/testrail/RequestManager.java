/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurock.testrail;

import java.util.concurrent.SynchronousQueue;

/**
 *
 * @author chreid
 */
public class RequestManager
{

    private static RequestManager instance = null;

    protected SynchronousQueue<Runnable> q = new SynchronousQueue<Runnable>();

    private RequestManager()
    {
        Thread consumer = new Thread("TestRailsRequestManagerConsumer")
        {
            public void run()
            {
                try
                {
                    Runnable run = q.take();
                    // wait for new item in Q
                    run.run();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        };
        consumer.start();
    }

    public static RequestManager getInstance()
    {
        if (instance == null)
        {
            instance = new RequestManager();
        }
        return instance;
    }

    public void manage(Runnable run)
    {
        q.add(run);
        // add the request to the q
        // try to run it, if it returns a 429
    }
}
