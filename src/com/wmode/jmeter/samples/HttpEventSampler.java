/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.samples;

import com.sun.net.httpserver.Headers;
import com.wmode.jmeter.protocol.http.control.HTTPEventSampleResult;
import com.wmode.jmeter.protocol.http.control.HttpEventCache;
import com.wmode.jmeter.protocol.http.control.HttpEventCacheType;
import com.wmode.jmeter.protocol.http.control.HttpEventHandler;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.function.BiConsumer;
import org.apache.jmeter.gui.tree.JMeterTreeNode;
import org.apache.jmeter.samplers.AbstractSampler;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.Interruptible;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.testelement.property.IntegerProperty;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author chreid
 */
public class HttpEventSampler extends AbstractSampler implements Interruptible {
    private static final Logger log = LoggingManager.getLoggerForClass();

    protected boolean doSampling = true;
    private transient JMeterTreeNode selectedNode = null;

    protected int DEFAULT_TIMEOUT = 30000;
    protected int DEFAULT_QUERY_INTERVAL = 250;

    protected static final String USE_PROTOCOL = "HttpEventSampler.use_protocol";
    protected static final String PROTOCOL = "HttpEventSampler.protocol";
    protected static final String USE_METHOD = "HttpEventSampler.use_method";
    protected static final String METHOD = "HttpEventSampler.method";
    protected static final String USE_REMOTEHOST = "HttpEventSampler.use_remoteHost";
    protected static final String REMOTEHOST = "HttpEventSampler.remoteHost";
    protected static final String USE_BODYCONTAINS = "HttpEventSampler.use_bodycontains";
    protected static final String BODYCONTAINS = "HttpEventSampler.bodycontains";
    protected static final String TIMEOUT = "HttpEventSampler.timeout";
    protected static final String QUERY_INTERVAL = "HttpEventSampler.queryinterval";

    public HttpEventSampler(){
        super();
        initTimeout(DEFAULT_TIMEOUT);
        initQueryInterval(DEFAULT_QUERY_INTERVAL);
    }

    public boolean getUseProtocol(){
        return getPropertyAsBoolean(USE_PROTOCOL);
    }
    public void setUseProtocol(boolean use){
        setProperty(USE_PROTOCOL, use);
    }
    public String getProtocol(){
        return getPropertyAsString(PROTOCOL);
    }
    public void setProtocol(String protocol){
        setProperty(PROTOCOL, protocol);
    }

    public boolean getUseMethod(){
        return getPropertyAsBoolean(USE_METHOD);
    }
    public void setUseMethod(boolean use){
        setProperty(USE_METHOD, use);
    }
    public String getMethod(){
        return getPropertyAsString(METHOD);
    }
    public void setMethod(String method){
        setProperty(METHOD, method);
    }

    public boolean getUseRemoteHost(){
        return getPropertyAsBoolean(USE_REMOTEHOST);
    }
    public void setUseRemoteHost(boolean use){
        setProperty(USE_REMOTEHOST, use);
    }
    public String getRemoteHost(){
        return getPropertyAsString(REMOTEHOST);
    }
    public void setRemoteHost(String remoteHost){
        setProperty(REMOTEHOST, remoteHost);
    }

    public boolean getUseBodyContains(){
        return getPropertyAsBoolean(USE_BODYCONTAINS);
    }
    public void setUseBodyContains(boolean use){
        setProperty(USE_BODYCONTAINS, use);
    }
    public String getBodyContains(){
        return getPropertyAsString(BODYCONTAINS);
    }
    public void setBodyContains(String remoteHost){
        setProperty(BODYCONTAINS, remoteHost);
    }

    protected void initTimeout(int timeout){
        setProperty(new IntegerProperty(TIMEOUT, timeout));
    }
    public int getTimeout(){
        return getPropertyAsInt(TIMEOUT);
    }
    public String getTimeoutAsString(){
        return getPropertyAsString(TIMEOUT);
    }
    public void setTimeout(int timeout){
        initTimeout(timeout);
    }

    protected void initQueryInterval(int interval){
        setProperty(new IntegerProperty(QUERY_INTERVAL, interval));
    }
    public int getQueryInterval(){
        return getPropertyAsInt(QUERY_INTERVAL);
    }
    public String getQueryIntervalAsString(){
        return getPropertyAsString(QUERY_INTERVAL);
    }
    public void setQueryInterval(int interval){
        initQueryInterval(interval);
    }
    
    @Override
    public SampleResult sample(Entry entry) {
        HTTPEventSampleResult res = new HTTPEventSampleResult();
        res.setMonitor(false);

        res.setSampleLabel(getName());

        // TODO: make this a config in the GUI
        String protocol = getUseProtocol() ? getProtocol() : null;
        String method = getUseMethod() ? getMethod() : null;
        String remoteHost = getUseRemoteHost() ? getRemoteHost(): null;
        String bodyContains = getUseBodyContains()? getBodyContains(): null;
        res.setHTTPMethod(method);

        res.sampleStart();
        res.setResponseCode("Did not recieve http event matching specified parameters in sample.");

        long timeout = System.currentTimeMillis() + getTimeout();
        int queryInteval = getQueryInterval();
        try
        {
            Map<String, Object> row = cacheGet(protocol, method, remoteHost, bodyContains);

            while(doSampling && row == null){
                row = cacheGet(protocol, method, remoteHost, bodyContains);
                try{
                    Thread.sleep(queryInteval);
                }catch (InterruptedException ex){
                    // wake up damn it!
                }
                boolean isTimedout = System.currentTimeMillis() >= timeout;
                if(isTimedout){
                    throw new TimeoutException("Waiting for event from EventServer timed out, no event was received.");
                }
            }
            res.latencyEnd();

            if (row != null){
                // we found it!
                res.setSuccessful(true);
                res.setResponseCodeOK();
                res.setResponseMessageOK();

                long mtime = (long)row.get("mtime");
                res.setTimeStamp(mtime);

                String requestedMethod = (String)row.get("method");
                res.setHTTPMethod(requestedMethod);

                try{
                    String requestedHost = (String)row.get("requestedHost");
                    int requestedPort = (int)row.get("requestedPort");
                    String requestedProtocol = (String)row.get("protocol");
                    res.setURL(new URL(requestedProtocol+"://"+requestedHost+":"+requestedPort));
                }catch(MalformedURLException mfue){
                    log.error("error with url", mfue);
                }

                // output the data
                byte[] bodyBlob = (byte[])row.get("requestBody");
                if (bodyBlob != null){
                    log.debug("bodyBlob("+bodyBlob.length+"): " + bodyBlob);
                    res.setResponseData(bodyBlob);
                    res.setSamplerData(new String(bodyBlob));
                }
                String headerseaders = (String)row.get("requestHeaders");
                String headers = getHeadersString(headerseaders);
                res.setResponseHeaders(headers);
            }
        }
        catch (ParseException ex){
            String message = "Error when parsing json data from database.";
            log.error(message, ex);
            res.setResponseCode("Exception: " + message + ":" + ex.getMessage());
        }
        catch (ClassNotFoundException cnfe){
            String message = "A class was not found! is sqlite-jdbc on the class path?";
            log.error(message, cnfe);
            res.latencyEnd();
            res.setResponseCode("Exception: " + message + ":" + cnfe.getMessage());
        }
        catch (SQLException sqle){
            String message = "there was a SQL error from the cache!";
            log.error(message, sqle);
            res.latencyEnd();
            res.setResponseCode("Exception: " + message + ":" + sqle.getMessage());
        }
        catch (TimeoutException toe){
            String message = "could not find value from cache, timeout";
            res.latencyEnd();
            log.debug(message);
            res.setResponseCode("Exception: " + message + ":" + toe.getMessage());
        }

        res.sampleEnd();

        return res;
    }

    protected String getHeadersString(String headersJson) throws ParseException{
        JSONObject obj = (JSONObject)new JSONParser().parse(headersJson);
        Headers headers = new Headers();
        headers.putAll(obj);
        StringBuilder sb = new StringBuilder();
        headers.forEach(new BiConsumer<String, List<String>>() {
            @Override
            public void accept(String name, List<String> u)
            {
                sb.append(name);
                sb.append(": ");
                sb.append(String.join("; ", u));
                sb.append("\r\n");
            }
        });

        return sb.toString();
    }

    public Map<String, Object> cacheGet(String protocol, String method, String hostName, String bodyContains) throws ClassNotFoundException, SQLException {
        log.debug("Querying cache using getOne().");
        Map<String, Object> row = null;
        HttpEventCache cache = HttpEventCache.getInstance(HttpEventCacheType.getMemoryInstance());
        try{
            row = cache.getOne(protocol, method, hostName, bodyContains);
        }catch(HttpEventCache.NoResulsExecption nre){
            // ruhro...
        }
        return row;
    }

    @Override
    public boolean interrupt()
    {
        doSampling = false;
        return true;
    }
    
}
