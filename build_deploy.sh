#!/bin/bash

/Applications/NetBeans/NetBeans\ 8.1.app/Contents/Resources/NetBeans/java/maven/bin/mvn install
cp target/chrinor-jmeter-plugins-1.0-SNAPSHOT.jar ~/devel/rogers_jmeter/jmeter/apache-jmeter-2.13/lib/ext/ChrinorJMeterPlugins.jar
cd /Users/chreid/devel/rogers_jmeter/jmeter
rm -rf ./logs/*
./gui.sh -p env/test.properties --loglevel INFO
