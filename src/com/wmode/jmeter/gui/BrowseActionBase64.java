
package com.wmode.jmeter.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import kg.apc.jmeter.gui.BrowseAction;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.jmeter.gui.GuiPackage;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

/**
 *
 * @author creid
 */
public class BrowseActionBase64 implements ActionListener {

    protected static final Logger log = LoggingManager.getLoggerForClass();
    protected final JTextField control;
    protected boolean isDirectoryBrowse = false;
    protected String lastPath = ".";

    public BrowseActionBase64(JTextField filename) {
        control = filename;
    }

    public BrowseActionBase64(JTextField filename, boolean isDirectoryBrowse) {
        control = filename;
        this.isDirectoryBrowse = isDirectoryBrowse;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = getFileChooser();
        if (chooser != null) {
            if(GuiPackage.getInstance() != null) {
                int returnVal = chooser.showOpenDialog(GuiPackage.getInstance().getMainFrame());
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    // get the file data
                    File file = chooser.getSelectedFile();
                    try{
                        byte[] bytes = IOUtils.toByteArray(new FileInputStream(file));
                        String base64Data = new String(Base64.encodeBase64(bytes), StandardCharsets.UTF_8);
                        control.setText(base64Data);
                    }catch(IOException ioe){
                        log.error("Oh noes! file reading broke something!", ioe);
                    }
                }
                lastPath = chooser.getCurrentDirectory().getPath();
            }
        }
    }

    protected JFileChooser getFileChooser() {
        JFileChooser ret = new JFileChooser(lastPath);
        if(isDirectoryBrowse) {
            ret.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        }

        return ret;
    }
}
