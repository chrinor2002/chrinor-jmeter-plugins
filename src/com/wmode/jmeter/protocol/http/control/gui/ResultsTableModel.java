/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.protocol.http.control.gui;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chreid
 */
public class ResultsTableModel extends AbstractTableModel
{
    protected ArrayList<LinkedHashMap<String, Object>> results;

    public ResultsTableModel(){
        super();
        this.results = new ArrayList();
    }

    public ResultsTableModel(ArrayList<LinkedHashMap<String, Object>> results){
        super();
        this.results = results;
        
    }

    @Override
    public int getRowCount()
    {
        return results.size();
    }

    @Override
    public int getColumnCount()
    {
        if(!results.isEmpty()){
            return results.get(0).size();
        }else{
            return 0;
        }
    }
    @Override
    public String getColumnName(int index){
        if(!results.isEmpty()){
            String label = new ArrayList<String>(results.get(0).keySet()).get(index);
            return label;
        }else{
            return "";
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        LinkedHashMap<String, Object> row = results.get(rowIndex);
        ArrayList<Object> values = new ArrayList(row.values());
        return values.get(columnIndex);
    }
    
}
