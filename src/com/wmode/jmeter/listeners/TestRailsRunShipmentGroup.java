/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.listeners;

import com.gurock.testrail.APIClient;
import com.gurock.testrail.APIException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.assertions.AssertionResult;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author chreid
 */
public class TestRailsRunShipmentGroup implements Runnable{
    private static final Logger log = LoggingManager.getLoggerForClass();

    public static int TOGGLE_LOGGIN_NONE = 2^0; // 0
    public static int TOGGLE_LOGGING_PASSES_INFO = 2^10; // 1
    public static int TOGGLE_LOGGING_PASSES_DETAILS = TOGGLE_LOGGING_PASSES_INFO | 2^11; // 
    public static int TOGGLE_LOGGING_FAILURE_INFO = 2^20;
    public static int TOGGLE_LOGGING_FAILURE_DETAILS = TOGGLE_LOGGING_FAILURE_INFO | 2^21;

    protected APIClient api = new APIClient();
    protected String testRailTestRunID;
    protected String testRailTestCaseID;
    protected int railsVerbosity;
    public ArrayList<TestRailsRunShipment> manifest = new ArrayList();

    public TestRailsRunShipmentGroup(String testRailTestRunID, String testRailTestCaseID){
        this.testRailTestRunID = testRailTestRunID;
        this.testRailTestCaseID = testRailTestCaseID;
    }

    public void setupAPI(String baseurl, String user, String pass){
        if (api == null)
        {
            api = new APIClient();
        }
        api.setBaseUrl(baseurl);
        api.setUser(user);
        api.setPassword(pass);
    }
    
    public boolean isVerbosity(int verboseToggle){
        return (verboseToggle & getVerbosity()) == verboseToggle;
    }
    public int getVerbosity(){
        return this.railsVerbosity;
    }
    public void setVerbosity(int verbosity){
        this.railsVerbosity = verbosity;
    }

    protected JSONObject makeAPIGet(String callpath) throws MalformedURLException, IOException, APIException
    {
        return (JSONObject) api.sendGet(callpath);
    }

    protected JSONObject makeAPIPost(String callpath, JSONObject data) throws MalformedURLException, IOException, APIException
    {
        return (JSONObject) api.sendPost(callpath, data);
    }

    public static String indentText(String input){
        return indentText(input, "    ");
    }
    public static String indentText(String input, String indent){
        // convert to a "<pre>" in markdown
        String[] lines = input.split("\r\n|\r|\n");
        for(int i = 0; i < lines.length; i++){
            lines[i] = indent + lines[i];
        }
        return StringUtils.join(lines, "\n");
    }

    @Override
    public void run()
    {
        final JSONObject jsonPostData = new JSONObject();

        int testRailResultCode = APIClient.TEST_STATUS_PASSED;
        StringBuilder testRailResultMessage = new StringBuilder();

        log.debug("STARTING to prepare result for TestRail TestRun " + testRailTestRunID + " and TestCase ID " + testRailTestCaseID + ".");

        JSONArray jsonSteps = new JSONArray();
        for(TestRailsRunShipment shipment : manifest)
        {
            SampleResult sampleResult = shipment.sampleResult;
            JSONObject jsonStep = new JSONObject();
            String[] labelParts = sampleResult.getSampleLabel().split("\\|");
            //jsonStep.put("content", sampleResult.getSampleLabel());
            jsonStep.put("content", labelParts[labelParts.length-1]);
            //String response = sampleResult.getResponseDataAsString();
            //jsonStep.put("expected", sampleResult.getSamplerData());
            //jsonStep.put("actual", response);

            jsonStep.put("status_id", APIClient.TEST_STATUS_PASSED);

            // See if the test should be posting or not
            log.debug("Jmeter test name: " + sampleResult.getThreadName() + "/" + sampleResult.getSampleLabel());

            // Get all the Assertion Results
            AssertionResult[] results = sampleResult.getAssertionResults();
            log.debug("Number of assertions is: " + results.length);

            // Set up the PASS/FAILURE info
            StringBuilder assersionMessages = new StringBuilder();
            int errorNumber = 0;
            
            assersionMessages.append("## Assertions:\n");
            for(AssertionResult result : results){

                assersionMessages.append("* ").append(result.getName());

                boolean resultHasFailed = result.isFailure() || result.isError();
                if (resultHasFailed)
                {
                    // FAIL the whole test
                    testRailResultCode = APIClient.TEST_STATUS_FAILED;

                    errorNumber++;
                    assersionMessages.append(" *FAILED*.");//.append("\n");
                    if (isVerbosity(TOGGLE_LOGGING_FAILURE_INFO)){
                        assersionMessages.append(" Failure message is:").append("\n\n");
                        assersionMessages.append(indentText(result.getFailureMessage(), "        ")).append("\n");
                    }

                    jsonStep.put("status_id", APIClient.TEST_STATUS_FAILED);
                }
                else
                {
                    assersionMessages.append(" *PASSED*.");

                }
                assersionMessages.append("\n");
            }


            String url = sampleResult.getUrlAsString();
            String responseCode = sampleResult.getResponseCode();

            boolean isPassed = testRailResultCode == APIClient.TEST_STATUS_PASSED;
            boolean isFailed = testRailResultCode == APIClient.TEST_STATUS_FAILED;
            if (isPassed)
            {
                testRailResultMessage.append("# ").append(labelParts[labelParts.length-1]).append(" *PASSED*, with response code of: ");
            }
            else
            {
                testRailResultMessage.append("# ").append(labelParts[labelParts.length-1]).append(" *FAILED*, with response code of: ");
            }
            testRailResultMessage.append(responseCode).append(".");

            if (
                (isPassed && isVerbosity(TOGGLE_LOGGING_PASSES_INFO))
                || (isFailed && isVerbosity(TOGGLE_LOGGING_FAILURE_INFO))
                ){
                testRailResultMessage.append("\nURL hit was: ");
                testRailResultMessage.append("[").append(url).append("](").append(url).append(")").append("\n");
            }
            
            if (
                (isPassed && isVerbosity(TOGGLE_LOGGING_PASSES_DETAILS))
                || (isFailed && isVerbosity(TOGGLE_LOGGING_FAILURE_DETAILS))
                ){
                StringBuilder assersionSent = new StringBuilder();
                assersionSent.append("## Sent:\n");
                assersionSent.append(indentText(sampleResult.getSamplerData(), "        ")).append("\n");
                assersionSent.append("\n");
                testRailResultMessage.append(assersionSent);

                StringBuilder assersionReceived = new StringBuilder();
                assersionReceived.append("## Received:\n");
                assersionReceived.append(indentText(sampleResult.getResponseDataAsString(), "        ")).append("\n");
                assersionReceived.append("\n");
                testRailResultMessage.append(assersionReceived);
            }

            if (
                (isPassed && isVerbosity(TOGGLE_LOGGING_PASSES_INFO))
                || (isFailed && isVerbosity(TOGGLE_LOGGING_FAILURE_INFO))
                ){
                testRailResultMessage.append(assersionMessages);
            }

            log.debug("Full result to post to TestRail: " + testRailResultCode + " : " + testRailResultMessage.toString());
            log.debug("Error count number was: " + errorNumber);
            testRailResultMessage.append("*******\n");
            jsonSteps.add(jsonStep);
        }
        jsonPostData.put("status_id", testRailResultCode);
        jsonPostData.put("comment", testRailResultMessage.toString());
        jsonPostData.put("custom_step_results", jsonSteps);

        // Perform the POST
        try
        {
            log.info("Attempting to post to TestRail TestRun " + testRailTestRunID + " and TestCase ID " + testRailTestCaseID + "...");
            log.debug("TestRun data being posted is: " + jsonPostData);
            final String path = "add_result_for_case/" + testRailTestRunID + "/" + testRailTestCaseID;

            try
            {
                JSONObject response = makeAPIPost(path, jsonPostData);
                log.info("Successfully completed the TestRail POST.  Response from the post to TestRail was: " + response.toJSONString());
            }
            catch (MalformedURLException e)
            {
                log.error("TESTRAIL POST FAILED There appears to be an invalid url, check configuration.", e);
            }
            catch (APIException e)
            {
                int code = e.getHTTPCode();
                switch (code)
                {
                    case 400:
                        log.error("TESTRAIL POST FAILED with error code 400! -- You may have an invalid format of the data being posted to TestRail, or be using an invalid TestRail test case ID.  Look into the log values and try to assess what may be invalid.");
                        //sampleResult.setSuccessful(false);
                        //sampleResult.setResponseMessage("400 response from TestRail - invalid format - look in logs");
                        break;

                    case 401:
                        log.error("TESTRAIL POST FAILED with error code 401! -- You may not have the right credentials to connect to the TestRail Post.  Ensure nothing was changed, or go to 'http://docs.gurock.com/testrail-api2/accessing' for further information on how to connect.");
                        //sampleResult.setSuccessful(false);
                        //sampleResult.setResponseMessage("401 response from TestRail - invalid auth - look in logs");
                        break;

                    case 403:
                        log.error("TESTRAIL POST FAILED with error code 403! -- You may be trying to use an invalid/old/closed TestRun ID.  Change that in the Jmeter \"TestRail Variables\".");
                        //sampleResult.setSuccessful(false);
                        //sampleResult.setResponseMessage("403 response from TestRail - invalid ID - look in logs");
                        break;

                    case 429:
                        //log.error("TESTRAIL POST FAILED with error code 429! -- There was a timeout on the post to TestRail, possibly related to too many posts too quickly.  Delays may be needed.");
                        log.warn("TESTRAIL POST FAILED with error code 429! -- exceded limit for throttling, queing for retry.");
                        String header = e.getHttpURLConnection().getHeaderField("Retry-After");
                        final int retryInSeconds = Integer.parseInt(header);
                        //int retryInSecondsTemp = 30;
                        //log.info("TestRail suggested wait time, from the 'Retry-After' header, is: " + retryInSeconds + " seconds, but actually waiting: " + retryInSecondsTemp + " seconds");

                        //sampleResult.setSuccessful(false);
                        //sampleResult.setResponseMessage("429 response from TestRail - timeout - look in logs");

                        /*TODO:  There is an issue when attempting to retry a post after the 60 second timeout, because the next test almost always fails.  This is 
                            related to Jmeter trying to re-use an idle connection (see comment 21 on https://bz.apache.org/bugzilla/show_bug.cgi?id=56119).  We can 
                            disable "keep-alive" on EVERY sample in every test, or we can reduce the timeout to something smaller than the suggested response from 
                            TestRail (currently 60 seconds).  
                         */

                        //Sleep and retry, but keep the test going.
                        // TODO: this really needs to be abstracted into a manager to prevent spinning up a ton of extra threads...
                        Runnable retry = new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                log.debug("Sleeping for " + retryInSeconds + " seconds...  zzzzzzzz...");
                                try
                                {
                                    Thread.sleep(1000 * retryInSeconds);
                                }
                                catch (InterruptedException ex)
                                {
                                    log.error(ex.getMessage(), ex);
                                }
                                log.debug("AWAKE!");
                                try
                                {
                                    JSONObject response = makeAPIPost(path, jsonPostData);
                                    log.info("Successfully completed the TestRail POST on the second attempt, for TestRun " + testRailTestRunID + " and TestCase ID " + testRailTestCaseID + "..." + "Response from the post to TestRail was: " + response.toJSONString());
                                }
                                catch (MalformedURLException e)
                                {
                                    log.error("TESTRAIL POST FAILED There appears to be an invalid url, check configuration.", e);
                                }
                                catch (APIException e)
                                {
                                    log.error("Still failed to post the TestRail response for TestRun " + testRailTestRunID + " and TestCase ID " + testRailTestCaseID + "...", e);
                                }
                                catch (Exception e)
                                {
                                    log.error(e.getMessage(), e);
                                }
                                finally
                                {
                                    log.info("INNER FINISHED with TestRail TestRun " + testRailTestRunID + " and TestCase ID " + testRailTestCaseID + ".");
                                }
                            }
                        };
                        new Thread(retry).start();
                        break;

                    default:
                        log.error(e.getMessage(), e);
                }
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
        finally
        {
            log.info("FINISHED with TestRail TestRun " + testRailTestRunID + " and TestCase ID " + testRailTestCaseID + ".");
        }
    }

}
