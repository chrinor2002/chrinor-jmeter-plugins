/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wmode.jmeter.protocol.http.control;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.sql.SQLException;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

/**
 *
 * @author chreid
 */
public class HttpEventHandler implements HttpHandler {
    private static final Logger log = LoggingManager.getLoggerForClass();

    protected int default_response_code;
    protected String default_response_message;
    private HttpEventCache cache;

    public HttpEventHandler(HttpEventCache cache){
        this(cache, HttpURLConnection.HTTP_OK, "{\"status\":\"OK\"}");
    }

    public HttpEventHandler(HttpEventCache cache, int code, String response){
        this.cache = cache;
        default_response_code = code;
        default_response_message = response;
    }

    @Override
    public void handle(HttpExchange t) throws IOException
    {
        log.debug("handling request from " + t.getRemoteAddress());
        String[] parts = t.getProtocol().split("/");
        String protocol = parts[0].toLowerCase();
        cache.put(protocol, t.getRequestMethod(), t.getRemoteAddress().getHostName(), t);

        respond(t);
    }

    protected String getBody(InputStream in) throws IOException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] buff = new byte[1024];
        while(in.available() > 0){
            int length = in.read(buff);
            bao.write(buff, 0, length);
        }
        in.close();
        bao.close();
        return bao.toString();
    }

    public void respond(HttpExchange t) throws IOException {
        String response = default_response_message;
        t.getResponseHeaders().add("Content-Type", "text/json");
        t.sendResponseHeaders(default_response_code, response.length());
        OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    public void respondError(HttpExchange t) throws IOException {
        String response = "{\"status\":\"Error\"}";
        t.getResponseHeaders().add("Content-Type", "text/json");
        t.sendResponseHeaders(500, response.length());
        OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    public void serverStopped(){
        try{
            int itemsRemoved = cache.clearAll();
        }
        catch (SQLException ex){
            log.error("Error de-initing cache from handler", ex);
        }
    }
    public void serverStarted(){
    }
}
